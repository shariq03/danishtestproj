﻿using System;
using Autofac;
using Autofac.Builder;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Reflection;
using MVC40_Knockout_App.Services;
using MVC40_Knockout_App.Repository;

namespace MVC40_Knockout_App.App_Start
{
    public static class AutofacConfig
    {
        public enum InstanceScope
        {
            InstancePerDependency,
            InstancePerLifetimeScope,
            InstancePerMatchingLifetimeScope,
            SingleInstance
        }


        /// <summary>
        /// Register autofac configuration
        /// </summary>
        public static void RegisterAutofac(ContainerBuilder builder)
        {

            builder.RegisterType<EmployeeRepository>().As<IEmployeeRepository>().InstancePerLifetimeScope();
            builder.RegisterType<LeaveApplicationRepository>().As<ILeaveApplicationRepository>().InstancePerLifetimeScope();
            builder.RegisterType<EmployeeInfoService>().As<IEmployeeInfoService>().InstancePerLifetimeScope();
            builder.RegisterType<LeaveApplicationService>().As<ILeaveApplicationService>().InstancePerLifetimeScope();

        }

    }
}