﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC40_Knockout_App.Models;

namespace MVC40_Knockout_App.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private List<EmployeeInfo> employees;
        public EmployeeRepository()
        {
            employees = new List<EmployeeInfo>
            {
                new EmployeeInfo{DeptName = "HR", Position="Manager", EmpName="Keith Richards", EmpNo=1, Salary=80000, DateOfBirth = DateTime.Parse("2000-12-03") },
                new EmployeeInfo{DeptName = "Executive", Position="CEO", EmpName="Mick Jagger", EmpNo=2, Salary=100000, DateOfBirth = DateTime.Parse("1969-01-01") },
                new EmployeeInfo{DeptName = "R&D", Position="Programmer", EmpName="Bill Wyman", EmpNo=3, Salary=85000, DateOfBirth = DateTime.Parse("1980-01-08") },
                new EmployeeInfo{DeptName = "R&D", Position="Product Owner", EmpName="Ronnie Wood", EmpNo=4, Salary=90000, DateOfBirth = DateTime.Parse("1999-12-01") },
                new EmployeeInfo{DeptName = "Sales", Position="Business Development Manager", EmpName="Charlie Watts", EmpNo=5, Salary=95000, DateOfBirth = null},
            };
        }
        public EmployeeRepository(List<EmployeeInfo> Employees)
        {
            employees = Employees;
        }
        public List<EmployeeInfo> GetAllEmployees()
        {
            return employees;
        }

        public EmployeeInfo GetEmployeebyId(int EmpNo)
        {
            return employees
                .Where(e => e.EmpNo == EmpNo).SingleOrDefault();
        }

        public bool UpdateEmployee(EmployeeInfo Employee)
        {
            var employeeInfo = employees
                             .Where(e => e.EmpNo == Employee.EmpNo).SingleOrDefault();

            if (employeeInfo == null) return false;
            employeeInfo = Employee;
            return true;

        }

        public void InsertEmployee(EmployeeInfo Employee)
        {
            employees.Add(Employee);
        }

        public bool DeleteEmployee(int EmpNo)
        {
            var employeeInfo = employees
                           .Where(e => e.EmpNo == EmpNo).SingleOrDefault();

            if (employeeInfo == null) return false;

            employees.Remove(employeeInfo);
            return true;
        }
    }
}