﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC40_Knockout_App.Models;

namespace MVC40_Knockout_App.Repository
{
    public interface IEmployeeRepository
    {
        List<EmployeeInfo> GetAllEmployees();

        EmployeeInfo GetEmployeebyId(int EmpNo);

        bool UpdateEmployee( EmployeeInfo Employee);

        void InsertEmployee(EmployeeInfo Employee);

        bool DeleteEmployee(int EmpNo);
    }
}