﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC40_Knockout_App.Models;

namespace MVC40_Knockout_App.Repository
{
    public interface ILeaveApplicationRepository
    {
        List<LeaveApplication> GetAllLeaveApplications();

        List<LeaveApplication> GetLeaveApplicationById(int EmpId); 
    }
}