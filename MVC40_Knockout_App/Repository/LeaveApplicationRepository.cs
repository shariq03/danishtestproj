﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC40_Knockout_App.Models;

namespace MVC40_Knockout_App.Repository
{
    public class LeaveApplicationRepository : ILeaveApplicationRepository
    {
        private List<LeaveApplication> leaves;
        public LeaveApplicationRepository()
        {
            leaves = new List<LeaveApplication>
            {
                new LeaveApplication{EmpNo=1, StartDate=DateTime.Now.AddDays(-1),EndDate=DateTime.Now.AddDays(1)},
                new LeaveApplication{EmpNo=2, StartDate=DateTime.Now.AddDays(-1),EndDate=DateTime.Now.AddDays(1)},
                new LeaveApplication{EmpNo=3, StartDate = DateTime.Now.AddDays(-1), EndDate = DateTime.Now.AddDays(1)},
                new LeaveApplication{EmpNo=4, StartDate=DateTime.Now.AddDays(-1),EndDate=DateTime.Now.AddDays(1)},
                new LeaveApplication{EmpNo=4, StartDate=DateTime.Now.AddDays(-2),EndDate=DateTime.Now.AddDays(-2)},
            };
        } 
        public List<LeaveApplication> GetAllLeaveApplications()
        {
            return leaves;
        }

        public List<LeaveApplication> GetLeaveApplicationById(int EmpNo)
        {
            return leaves
             .Where(e => e.EmpNo == EmpNo).ToList();
        }

    }
}