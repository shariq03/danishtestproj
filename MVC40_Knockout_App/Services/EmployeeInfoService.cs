﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC40_Knockout_App.Models;
using MVC40_Knockout_App.Repository;

namespace MVC40_Knockout_App.Services
{
    public class EmployeeInfoService : IEmployeeInfoService
    {
        private readonly EmployeeRepository _employeeRepository;
        private List<EmployeeInfo> employees;
        public EmployeeInfoService()
        {
            _employeeRepository = new EmployeeRepository();
            employees = new List<EmployeeInfo>
            {
                new EmployeeInfo{DeptName = "HR", Position="Manager", EmpName="Keith Richards", EmpNo=1, Salary=80000, DateOfBirth = DateTime.Parse("2000-12-03") },
                new EmployeeInfo{DeptName = "Executive", Position="CEO", EmpName="Mick Jagger", EmpNo=2, Salary=100000, DateOfBirth = DateTime.Parse("1969-01-01") },
                new EmployeeInfo{DeptName = "R&D", Position="Programmer", EmpName="Bill Wyman", EmpNo=3, Salary=85000, DateOfBirth = DateTime.Parse("1980-01-08") },
                new EmployeeInfo{DeptName = "R&D", Position="Product Owner", EmpName="Ronnie Wood", EmpNo=4, Salary=90000, DateOfBirth = DateTime.Parse("1999-12-01") },
                new EmployeeInfo{DeptName = "Sales", Position="Business Development Manager", EmpName="Charlie Watts", EmpNo=5, Salary=95000, DateOfBirth = null},
            };
        }

        public EmployeeInfoService(EmployeeRepository EmployeeRepository)
        {
            _employeeRepository = EmployeeRepository;
        }

        public List<EmployeeInfo> Employees { get { return _employeeRepository.GetAllEmployees(); } }

        public EmployeeInfo FindEmployee(int id)
        {
            return _employeeRepository.GetEmployeebyId(id);
        }

        public bool UpdateEmployee(EmployeeInfo employee)
        {
           return _employeeRepository.UpdateEmployee(employee);
        }

        public void InsertEmployee(EmployeeInfo employee)
        {
            _employeeRepository.InsertEmployee(employee);
        }

        public void DeleteEmployee(int EmpNo)
        {
            _employeeRepository.DeleteEmployee(EmpNo);
        }
    }
}
