﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MVC40_Knockout_App.Models;

namespace MVC40_Knockout_App.Services
{
   public interface IEmployeeInfoService
    {
        List<EmployeeInfo> Employees { get; }
        EmployeeInfo FindEmployee(int id);
        bool UpdateEmployee(EmployeeInfo employee);
        void InsertEmployee(EmployeeInfo employee);
        void DeleteEmployee(int id);

    }
}
