﻿using MVC40_Knockout_App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVC40_Knockout_App.Services
{
    public interface ILeaveApplicationService
    {
        List<LeaveApplication> LeaveApplications { get; }

        List<LeaveApplication> FindEmployeeLeave(int id);

        List<LeaveApplicationView> LeaveApplicationViews();
    }
}
